@section('title')
Edit Data
@endsection

@section('page-level-css')
@endsection

@extends('layout')

@section('content')

<div class="container">
  	<div class="col-12">
	  <form method="post" action="{{route('studentsData.update',['id' => $student_data->id])}}">
  		@csrf
  		  <div class="row">
	  		  <div class="col-2 d-contents">
		  		  <a href="{{route('studentsData.index')}}"><i class="fas fa-2x fa-long-arrow-alt-left"></i></a>
	  		  </div>
	  		  <div class="col-4">
			  	  <h2>Edit Data</h2>
		  	  </div>
	  	  </div>
	  	  <div class="row">
	  	  	<div class="col-12">
		  	  	@include('partials.flashMessage')
	  	  	</div>
	  	  </div>
		  <div class="row">
	  	  	<div class="col-6">
			    <div class="form-group">
			      <label >Full Name</label>
			      <input type="text" class="form-control" placeholder="Enter Full Name" value="{{$student_data->full_name}}" name="full_name">
			      @error('full_name')
			          <div class="errorIn">{{$message}}</div>
			      @enderror
			    </div>
			</div>

			<div class="col-6">
			    <div class="form-group">
			      <label >Email address</label>
			      <input type="email" class="form-control" placeholder="Enter email" value="{{$student_data->email}}" name="email">
			      @error('email')
			          <div class="errorIn">{{$message}}</div>
			      @enderror
			    </div>
			</div>

			<div class="col-6">
			    <div class="form-group">
			      <label >Contact Number</label>
			      <input type="text" class="form-control" placeholder="Enter Contact Number" value="{{$student_data->contact_number}}" name="contact_number">
			      @error('contact_number')
			          <div class="errorIn">{{$message}}</div>
			      @enderror
			    </div>
			</div>

			<div class="col-6">
			    <div class="form-group">
			      <label >Gender</label>
			      <select  class="form-control" name="gender">
			        <option value="male" @if($student_data->gender == 'male') selected @endif >Male</option>
			        <option value="female" @if($student_data->gender == 'female') selected @endif >Female</option>
			      </select>
			      @error('gender')
			          <div class="errorIn">{{$message}}</div>
			      @enderror
			    </div>
			</div>

			<div class="col-6">
			    <div class="form-group">
			      <label >City</label>
			      <input type="text" class="form-control" placeholder="Enter city" value="{{$student_data->city}}" name="city">
			    </div>
			</div>

			<div class="col-6">
			    <div class="form-group">
			      <label >Higher Education</label>
			      <input type="text" class="form-control" placeholder="Enter Higher Education" value="{{$student_data->higher_education}}" name="higher_education">
			    </div>
			</div>

			<div class="col-12">
			    <div class="form-group">
			      <label >Address</label>
			      <textarea type="text" class="form-control" placeholder="Enter Address" name="address">{{$student_data->address}}</textarea>
			    </div>
			</div>
		    <div class="col-4">
		    	<button type="submit" class="btn btn-success btn-block">Update</button>
			</div>
		</div>
	  </form>

  </div>
</div>

@endsection

@section('page-level-js')

@endsection