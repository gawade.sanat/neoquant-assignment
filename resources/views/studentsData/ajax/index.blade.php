@foreach( $students as $student )
  <div class="col-lg-4 col-sm-6 col-12 main-section text-center">
      <div class="row">
          <div class="col-lg-12 col-sm-12 col-12 profile-header"></div>
      </div>
      <div class="row user-detail">
          <div class="col-lg-12 col-sm-12 col-12">
          	  @if($student->gender == 'male')
                  <img src="https://www.nicesnippets.com/demo/man01.png" class="rounded-circle img-thumbnail">
              @else
                  <img src="https://cdn1.iconfinder.com/data/icons/avatars-1-5/136/87-512.png" class="rounded-circle img-thumbnail">
              @endif
              <h5>{{$student->full_name}}</h5>
              <p><i class="fa fa-map-marker" aria-hidden="true"></i> {{$student->address}}</p>

              <hr>
              <a  href="#student_detail_modal" onclick="process(this)" class="btn btn-sm" data-process-type = 'view' data-id ="{{$student->id}}" data-full-name = "{{$student->full_name}}" data-full-email = "{{$student->email}}" data-contact = "{{$student->contact_number}}" data-gender = "{{$student->gender}}" data-address = "{{$student->address}}" data-city = "{{$student->city}}" data-higher-education = "{{$student->higher_education}}">
              	<span style="font-size: 1.5em; color: #17a2b8	;">
              		<i class="fas fa-eye"></i>
              	</span>
              </a>
              <a href="{{route('studentsData.edit',['id' => $student->id])}}" class="btn btn-sm">
              	<span style="font-size: 1.5em; color: DodgerBlue;">
                  	<i class="fas fa-edit"></i>
                  </span>
              </a>
              <a href="#student_delete_modal" onclick="process(this)" class="btn btn-sm" data-process-type = 'delete' data-id ="{{$student->id}}">
              	<span style="font-size: 1.5em; color: Tomato;">
              		<i class="fas fa-trash-alt"></i>
              	</span>
              </a>
              <hr>
              
          </div>
      </div>
      
  </div>
@endforeach