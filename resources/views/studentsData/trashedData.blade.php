@section('title')
Student's Trashed Data
@endsection

@section('page-level-css')
@endsection

@extends('layout')

@section('content')

<div class="container">
  	@if(count($students) != 0)
  	<div class="row">
	  <form method="POST" action="{{route('studentsData.restore')}}" enctype="multipart/form-data">
		<div class="upload-btn-wrapper mb-2">
	  	  @csrf
		  <input type="hidden" name="restore_all" value="1" />
		</div>
		<div class="mb-2">
		 <button type="submit" class="btn btn-success">Restore All Data</button>
		</div>
	  </form>
  	</div>
	@include('partials.flashMessage')
    <div class="row">
     
	  @foreach( $students as $student )
	      <div class="col-lg-4 col-sm-6 col-12 main-section text-center">
	          <div class="row">
	              <div class="col-lg-12 col-sm-12 col-12 profile-header"></div>
	          </div>
	          <div class="row user-detail">
	              <div class="col-lg-12 col-sm-12 col-12">
	              	  @if($student->gender == 'male')
		                  <img src="https://www.nicesnippets.com/demo/man01.png" class="rounded-circle img-thumbnail">
	                  @else
		                  <img src="https://cdn1.iconfinder.com/data/icons/avatars-1-5/136/87-512.png" class="rounded-circle img-thumbnail">
	                  @endif
	                  <h5>{{$student->full_name}}</h5>
	                  <p><i class="fa fa-map-marker" aria-hidden="true"></i> {{$student->address}}</p>

	                  <hr>
	                  <a  href="#student_detail_modal" onclick="process(this)" class="btn btn-sm" data-process-type = 'view' data-id ="{{$student->id}}" data-full-name = "{{$student->full_name}}" data-full-email = "{{$student->email}}" data-contact = "{{$student->contact_number}}" data-gender = "{{$student->gender}}" data-address = "{{$student->address}}" data-city = "{{$student->city}}" data-higher-education = "{{$student->higher_education}}">
	                  	<span style="font-size: 1.5em; color: #17a2b8	;">
	                  		<i class="fas fa-eye"></i>
	                  	</span>
	                  </a>
	                  <a href="#student_delete_modal" onclick="process(this)" class="btn btn-sm" data-process-type = 'restore' data-id ="{{$student->id}}">
	                  	<span style="font-size: 1.5em; color: DodgerBlue;">
		                  	<i class="fas fa-trash-restore"></i>
		                  </span>
	                  </a>
	                  <hr>
	                  
	              </div>
	          </div>
	          
	      </div>
      @endforeach
     @else
	     <div class="row">
	     	
		     	<div class="code-area">
		     	  <span style="color: #777;font-style:italic;">
		     	    // Trash Can is empty.
		     	  </span>
		     	  <span>
		     	    <span style="color:#d65562;">
		     	      if
		     	    </span>
		     		  (<span style="color:#4ca8ef;">!</span><span style="font-style: italic;color:#bdbdbd;">found</span>)
		     	    {
		     	  </span>
		     	  <span>
		     	    <span style="padding-left: 15px;color:#2796ec">
		     	       <i style="width: 10px;display:inline-block"></i>throw
		     	    </span>
		     	    <span>
		     	      (<span style="color: #a6a61f">" (╯°□°)╯ "</span>);
		     	    </span>
		     		  <span style="display:block">}</span>
		     		  <span style="color: #777;font-style:italic;">
		     			  // <a href="{{route('studentsData.index')}}">Go home!</a>
		     		  </span>
		     	  </span>
		     	</div>
	     	
	     </div>
     @endif
    </div>
  </div>
	<div id="student_detail_modal" class="overlay">
		<div class="popup">
			<h2 id="full_name" class="text-center"></h2>
			<a class="close" href="#">&times;</a>
			<div class="content">
				<p id="gender"></p>
				<p id="higher_education"></p>
				<p id="email"></p>
				<p id="contact_number"></p>
				<p id="address"></p>
				<p id="city"></p>
				
			</div>
		</div>
	</div>
    <div id="student_delete_modal" class="overlay">
    	<div class="popup">
    		<a class="close" href="#">&times;</a>
    		<div class="content">
    			<p>Are you sure you want to restore this.?</p>
    			<form id="delete_detail_form" method="POST">
    			@csrf
    			<input type="hidden" name="id" id="data_id">
  	  			<div class="text-right">
  	  				<button type="submit" class="btn btn-success">Restore</button>
  	  			</div>
    			</form>
    		</div>
    	</div>
    </div>
@endsection

@section('page-level-js')
<script type="text/javascript">
	function process(event) {
		var logic =  event.getAttribute('data-process-type');
		if (logic == 'view') {
			$('#full_name').text(event.getAttribute('data-full-name'));
			$('#email').text(event.getAttribute('data-full-email'));
			$('#contact_number').text(event.getAttribute('data-contact'));
			$('#gender').text(event.getAttribute('data-gender'));
			$('#address').text(event.getAttribute('data-address'));
			$('#city').text(event.getAttribute('data-city'));
			$('#higher_education').text(event.getAttribute('data-higher-education'));
		}else{
			var id = event.getAttribute('data-id');
			var url = '{{url('/')}}/students-data/restore';
			$('#data_id').val(id);
			$('#delete_detail_form').attr('action', url);
		}
	}
</script>
@endsection