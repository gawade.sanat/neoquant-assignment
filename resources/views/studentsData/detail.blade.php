@section('title')
Students Data
@endsection

@section('page-level-css')
@endsection

@extends('layout')

@section('content')

<div class="container">
  	<div class="row">
  		  <form method="POST" action="{{route('studentsData.upload')}}" enctype="multipart/form-data">
  			<div class="upload-btn-wrapper mb-2">
  		  	  @csrf
	  		  <button class="upload-btn-custom">Upload a file</button>
	  		  <input type="file" name="file" />
  			</div>
  			<div class="mb-2">
	  		 <button type="submit" class="btn btn-success">Submit</button>
	  		</div>
  		  </form>
  	</div>
  	@include('partials.flashMessage')

  	@if(isset($querry))
  		<h2>Search results for: "{{$querry}}"</h2>
  	@endif
  	<!--Filter-->
  	<nav class="navbar navbar-light light-blue lighten-4">
  	  <a class="navbar-brand" href="#"></a>
  	  <button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1"
  	    aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"><span class="dark-blue-text"><i class="fas fa-filter"></i></span></button>
  	  <div class="collapse navbar-collapse" id="navbarSupportedContent1">
  		  <form method="get" action="{{route('studentsData.result')}}">
  		    <div class="row">
  		      <div class="col-10">
  		        <input type="text" class="form-control" placeholder="Universal Search" name="querry">
  		      </div>
  		      <div class="col-2">
  		        <button type="submit" class="btn btn-success"><i class="fas fa-search"></i></button>
  		      </div>
  		    </div>
  		  </form>
  	  </div>
  	</nav>
  	<!--/.Filter-->
    <div class="row posts endless-pagination" data-next-page="{{ $students->nextPageUrl() }}" >
		  @foreach( $students as $student )
		      <div class="col-lg-4 col-sm-6 col-12 main-section text-center">
		          <div class="row">
		              <div class="col-lg-12 col-sm-12 col-12 profile-header"></div>
		          </div>
		          <div class="row user-detail">
		              <div class="col-lg-12 col-sm-12 col-12">
		              	  @if($student->gender == 'male')
			                  <img src="https://www.nicesnippets.com/demo/man01.png" class="rounded-circle img-thumbnail">
		                  @else
			                  <img src="https://cdn1.iconfinder.com/data/icons/avatars-1-5/136/87-512.png" class="rounded-circle img-thumbnail">
		                  @endif
		                  <h5>{{$student->full_name}}</h5>
		                  <p><i class="fa fa-map-marker" aria-hidden="true"></i> {{$student->address}}</p>

		                  <hr>
		                  <a  href="#student_detail_modal" onclick="process(this)" class="btn btn-sm" data-process-type = 'view' data-id ="{{$student->id}}" data-full-name = "{{$student->full_name}}" data-full-email = "{{$student->email}}" data-contact = "{{$student->contact_number}}" data-gender = "{{$student->gender}}" data-address = "{{$student->address}}" data-city = "{{$student->city}}" data-higher-education = "{{$student->higher_education}}">
		                  	<span style="font-size: 1.5em; color: #17a2b8	;">
		                  		<i class="fas fa-eye"></i>
		                  	</span>
		                  </a>
		                  <a href="{{route('studentsData.edit',['id' => $student->id])}}" class="btn btn-sm">
		                  	<span style="font-size: 1.5em; color: DodgerBlue;">
			                  	<i class="fas fa-edit"></i>
			                  </span>
		                  </a>
		                  <a href="#student_delete_modal" onclick="process(this)" class="btn btn-sm" data-process-type = 'delete' data-id ="{{$student->id}}">
		                  	<span style="font-size: 1.5em; color: Tomato;">
		                  		<i class="fas fa-trash-alt"></i>
		                  	</span>
		                  </a>
		                  <hr>
		                  
		              </div>
		          </div>
		          
		      </div>
	      @endforeach
    </div>
  </div>

  <div id="student_detail_modal" class="overlay">
  	<div class="popup">
  		<h2 id="full_name" class="text-center"></h2>
  		<a class="close" href="#">&times;</a>
  		<div class="content">
  			<p id="gender"></p>
  			<p id="higher_education"></p>
  			<p id="email"></p>
  			<p id="contact_number"></p>
  			<p id="address"></p>
  			<p id="city"></p>
  			
  		</div>
  	</div>
  </div>

  <div id="student_delete_modal" class="overlay">
  	<div class="popup">
  		<a class="close" href="#">&times;</a>
  		<div class="content">
  			<p>Are you sure you want to delete this.?</p>
  			<form id="delete_detail_form" method="POST">
  				@csrf
	  			<div class="form-group form-check">
	  			    <input type="checkbox" class="form-check-input" id="skip_trash" name="skip_trash">
	  			    <label class="form-check-label">Skip the trash.!</label>
	  			    <p id="warning-message" class="errorIn"></p>
	  			</div>
	  			<div class="text-right">
	  				<button type="submit" class="btn btn-danger">Delete</button>
	  			</div>
  			</form>
  		</div>
  	</div>
  </div>
@endsection

@section('page-level-js')
<script type="text/javascript">
	function process(event) {
		var logic =  event.getAttribute('data-process-type');
		if (logic == 'view') {
			$('#full_name').text(event.getAttribute('data-full-name'));
			$('#email').text(event.getAttribute('data-full-email'));
			$('#contact_number').text(event.getAttribute('data-contact'));
			$('#gender').text(event.getAttribute('data-gender'));
			$('#address').text(event.getAttribute('data-address'));
			$('#city').text(event.getAttribute('data-city'));
			$('#higher_education').text(event.getAttribute('data-higher-education'));
		}else{
			var id = event.getAttribute('data-id');
			var url = '{{url('/')}}/students-data/delete' + '/' + id;
			$('#delete_detail_form').attr('action', url);
		}
	}

	$( document ).ready(function() {
	    $('#skip_trash').click(function(){
	    	if ($(this).is(':checked')) {
		    	$('#warning-message').text('By skipping the trash, you cannot retrieve the data.!');
	    	}else{
		    	$('#warning-message').text('');
	    	}
	    });

	    $(window).scroll(fetchPosts);
	    function fetchPosts() {
	        var page = $('.endless-pagination').data('next-page');
	        if(page !== null) {
	            clearTimeout( $.data( this, "scrollCheck" ) );
	            $.data( this, "scrollCheck", setTimeout(function() {
	                var scroll_position_for_posts_load = $(window).height() + $(window).scrollTop() + 100;
	                if(scroll_position_for_posts_load >= $(document).height()) {
	                    jQuery.get(page, function(data){
	                        $('.posts').append(data.students);
	                        $('.endless-pagination').data('next-page', data.next_page);
	                    });
	                }
	            }, 350))
	        }
	    }
	});
</script>
@endsection