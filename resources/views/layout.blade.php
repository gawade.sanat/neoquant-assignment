<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    {{-- main.css starts here --}}
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
    {{-- main.css ends here --}}

    {{-- page level css starts here --}}
    @yield('page-level-css')
    {{-- page level css ends here --}}
    
    {{-- font awsome stats here --}}
    <script src="https://kit.fontawesome.com/ad03b00714.js" crossorigin="anonymous"></script>
    {{-- font awsome ends here --}}
    
    <title>@yield('title')</title>

  </head>
  <body>
    
    {{-- header starts here --}}
    @include('partials.header')
    {{-- header ends here --}}
    
    {{-- content starts here --}}
    @yield('content')
    {{-- content starts here --}}

    {{-- footer starts here --}}
    @include('partials.footer')
    {{-- footer ends here --}}

    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    {{-- main.js starts here --}}
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>
    {{-- main.js ends here --}}

    {{-- page level js starts here --}}
    @yield('page-level-js')
    {{-- page level js ends here --}}

  </body>
</html>