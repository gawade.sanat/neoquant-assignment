@if(Session::has('success'))
<div class="mt-3 mb-3 alert alert-success alert-dismissible fade show" role="alert">
  <strong></strong> {{Session::get('success')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if(Session::has('error'))
<div class="mt-3 mb-3 alert alert-danger alert-dismissible fade show" role="alert">
  <strong></strong> {{Session::get('error')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif