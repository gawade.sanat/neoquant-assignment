<?php

namespace App\Imports;

use App\Student;
use Maatwebsite\Excel\Concerns\ToModel;

class StudentImport implements ToModel
{
    public function model(array $row)
    {
        return new Student([
           'full_name'     => $row[0],
           'email'    => $row[1],
           'contact_number'    => $row[2],
           'gender'    => $row[3],
           'address'    => $row[4],
           'city'    => $row[5],
           'higher_education'    => $row[6],
        ]);
    }
}
