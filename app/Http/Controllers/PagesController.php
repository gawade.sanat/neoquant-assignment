<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
    	return view('welcome');
    }

    public function studentsData(Request $request)
    {	
    	// get the students data
    	$students = Student::paginate(6);

    	if($request->ajax()) {
            return [
                'students' => view('studentsData.ajax.index')->with(compact('students'))->render(),
                'next_page' => $students->nextPageUrl()
            ];
        }
        
    	return view('studentsData.detail')->with(compact(['students']));
    }

    public function studentsDataEdit($id)
    {	
    	// get student data
    	$student_data = Student::where(['id' => $id])->first();
    	return view('studentsData.edit')->with(compact(['student_data']));
    }

    public function trashCan()
    {
    	$students = Student::onlyTrashed()->paginate(6);
    	return view('studentsData.trashedData')->with(compact(['students']));
    }
}
