<?php

namespace App\Http\Controllers;

use App\Imports\StudentImport;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use DB;
use Excel;
use Validator;
class StudentController extends Controller
{	
	public function studentsDataUpload(Request $request)
    {	
    	if($request->hasFile('file')){
	    	$file = $request->file('file');

	    	$import = Excel::import(new StudentntImport, $file);
			if ($import) {
		    	return redirect()->back()->with('success','Student data excel uploaded successfully');
			}else{
				return redirect()->back()->with('error','Whoops.! Server under maintainance, try again later.');
			}
    	}else{
    		return redirect()->back()->with('error','Whoops.! Need a file to upload.');
    	}
    	
    }

    public function studentsDataUpdate(Request $request,$id)
    {	
    	// validatiom
    	$validator = Validator::make($request->all(), [
    	            'full_name' => 'required',
    	            'email' => ['required',
    	            Rule::unique('students')->ignore($id),
    	        	],
    	            'contact_number' => 'required',
    	            'gender' => 'required'
    	        ]);
    	// if validation fails
    	if ($validator->fails()) {
    	           return redirect()->back()->withErrors($validator)->withInput();
    	       }
    	// save
    	$update_student_data = Student::find($id);
    	$update_student_data->full_name = $request->full_name;
    	$update_student_data->email = $request->email;
    	$update_student_data->contact_number = $request->contact_number;
    	$update_student_data->gender = $request->gender;
    	$update_student_data->address = $request->address;
    	$update_student_data->city = $request->city;
    	$update_student_data->higher_education = $request->higher_education;
    	$check_update = $update_student_data->save();

    	// redirect with flash message
    	if ($check_update) {
    		return redirect()->back()->with('success','Student data updated successfully');
    	}else{
    		return redirect()->back()->with('error','Whoops.! Server under maintainance, try again later.');
    	}
    }

    public function studentsDataDelete(Request $request, $id)
    {
    	// return $request->all();
    	if (isset($request->skip_trash)) {
    		$delete = Student::find($id)->forceDelete();
    	}else{
    		$delete = Student::find($id)->delete();	
    	}

    	if ($delete) {
    		return redirect()->back()->with('success','Student data deleted successfully');
    	}else{
    		return redirect()->back()->with('error','Whoops.! Server under maintainance, try again later.');
    	}
    }

    public function studentsDataRestore(Request $request)
    {
    	if (isset($request->restore_all)) {
    		$restore = Student::onlyTrashed()->restore();
    		// $restore = $get_trash_data->restore();
    	}else{
    		$restore = Student::withTrashed()->where(['id' => $request->id])->restore();
    	}

    	if ($restore) {
    		return redirect()->back()->with('success','Student data restored successfully');
    	}else{
    		return redirect()->back()->with('error','Whoops.! Server under maintainance, try again later.');
    	}
    }

    public function studentsDataResult(Request $request)
    {
    	$querry = $request->querry;

    	$students = Student::where('full_name', 'LIKE', '%'.$querry.'%')
    	                ->orWhere('email', 'LIKE', '%'.$querry.'%')
    	                ->orWhere('contact_number', 'LIKE', '%'.$querry.'%')
    	                ->orWhere('city', 'LIKE', '%'.$querry.'%')
    	                ->orWhere('higher_education', 'LIKE', '%'.$querry.'%')
    	                ->paginate(6);

    	if($request->ajax()) {
            return [
                'students' => view('studentsData.ajax.index')->with(compact('students'))->render(),
                'next_page' => $students->nextPageUrl()
            ];
        }
    	return view('studentsData.detail')->with(compact(['students','querry']));
    }
    
}
