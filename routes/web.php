<?php

Route::get('/','PagesController@index')->name('home');
// students crud starts here
Route::get('students-data-center','PagesController@studentsData')->name('studentsData.index');
Route::get('students-data/{id}/edit','PagesController@studentsDataEdit')->name('studentsData.edit');

Route::post('students-data/upload','StudentController@studentsDataUpload')->name('studentsData.upload');
Route::post('students-data/update/{id}','StudentController@studentsDataUpdate')->name('studentsData.update');
Route::post('students-data/delete/{id}','StudentController@studentsDataDelete')->name('studentsData.delete');

Route::post('students-data/restore','StudentController@studentsDataRestore')->name('studentsData.restore');
Route::get('trash-can','PagesController@trashCan')->name('trashCan.index');

Route::get('students-data/result','StudentController@studentsDataResult')->name('studentsData.result');
// students crud ends here
